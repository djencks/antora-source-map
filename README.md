# Antora Source-Map Extension
:version: 0.0.1-rc.2

This is an Antora extension that edits content sources and branches to ease local and PR builds.

See the more complete README.adoc at gitlab.
