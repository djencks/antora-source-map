'use strict'

module.exports.register = function ({ config }) {
  const logger = this.getLogger('@djencks/antora-source-map')
  config.logLevel && (logger.level = config.logLevel)

  const sourceList = config.sourceMap || []
  const sourceMap = sourceList.reduce((accum, source) => {
    accum[source.url] = {
      mappedUrl: source.mappedUrl,
      branches: source.branches.reduce((accum, b) => {
        accum[b.branch] = b.mappedBranch
        return accum
      }, {}),
    }
    return accum
  }, {})
  logger.trace({ msg: 'source-map', sourceMap })
  this.prependListener('playbookBuilt',
    ({ playbook }) => {
      const sources = playbook.content.sources.reduce((accum, source) => {
        const url = source.url
        const map = sourceMap[url]
        if (map) {
          const sourceBranches = source.branches
            ? Array.isArray(source.branches)
              ? source.branches : [source.branches]
            : source.branch ? [source.branch] : []
          const mappedBranches = []
          const unmappedBranches = []
          sourceBranches.forEach((branch) => {
            if (map.branches[branch]) {
              mappedBranches.push(map.branches[branch])
            } else {
              unmappedBranches.push(branch)
            }
          })
          mappedBranches.length && accum.push(Object.assign({}, source, { url: map.mappedUrl },
            { branches: mappedBranches }))
          unmappedBranches.length && accum.push(Object.assign({}, source,
            { branches: unmappedBranches }))
        } else {
          accum.push(source)
        }
        return accum
      }, [])

      logger.debug(sources)

      this.updateVariables({
        playbook: Object.assign({}, playbook,
          { content: Object.assign({}, playbook.content, { sources }) }),
      })
    }
  )
}
