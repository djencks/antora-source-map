/* eslint-env mocha */
'use strict'

const { expect } = require('chai')

const EventEmitter = require('events')
const camelCaseKeys = require('camelcase-keys')
const sourceMap = require('./../lib/source-map')
const yaml = require('yaml')

describe('source-map tests', () => {
  let eventEmitter
  let vars
  let log

  beforeEach(() => {
    eventEmitter = new EventEmitter()
    log = []
    sourceMap.on = (event, code) => {
      eventEmitter.on(event, code)
    }
    sourceMap.prependListener = (event, code) => {
      eventEmitter.on(event, code)
    }
    sourceMap.updateVariables = (vars_) => {
      vars = vars_
    }
    sourceMap.getLogger = (name) => {
      return {
        trace: (msg) => {
          log.push(msg)
        },
        debug: (msg) => {
          log.push(msg)
        },
        info: (msg) => {
          log.push(msg)
        },
        isLevelEnabled: (level) => true,
      }
    }
  })

  it('basic test', () => {
    sourceMap.register({ config: {} })
    expect(eventEmitter.eventNames().length).to.equal(1)
  })

  const playbook1 = `content:
  sources:

    - url: https://github.com/apache/camel.git
      branches: main
      start_paths:
        # manual
        - docs/user-manual

    - url: https://github.com/apache/camel.git
      branches:
        - main
        - camel-3.13.x
        - camel-3.12.x
        - camel-3.11.x
        - camel-3.7.x
      start_paths:
        # eip
        - core/camel-core-engine/src/main/docs
        # main components doc
        - docs/components

    - url: https://github.com/apache/camel.git
      branches: camel-2.x
      start_paths:
        # main components doc
        - docs/components
        # languages
        - camel-core/src/main/docs

# sub-projects
    - url: https://github.com/apache/camel-k.git
      branches:
        - main
        - release-1.7.x
        - release-1.6.x
        - release-1.4.x
      start_path: docs

    - url: https://github.com/apache/camel-k-runtime.git
      branches:
        - main
        - release-1.10.x
        - release-1.9.x
      start_path: docs

    - url: https://github.com/apache/camel-kamelets.git
      branches:
        - main
        - 0.5.x
      start_path: docs

    - url: https://github.com/apache/camel-quarkus.git
      branches:
        - main
        - 2.5.x
        - 2.4.x
      start_path: docs

    - url: https://github.com/apache/camel-quarkus-examples.git
      branches: main
      start_path: docs

    - url: https://github.com/apache/camel-kafka-connector.git
      branches:
        - main
        - camel-kafka-connector-0.11.x
      start_paths:
        - docs
        - connectors

    - url: https://github.com/apache/camel-spring-boot.git
      branches:
        - main
        - camel-spring-boot-3.13.x
        - camel-spring-boot-3.12.x
        - camel-spring-boot-3.11.x
        - camel-spring-boot-3.7.x
      start_paths:
        - components-starter
        - core
        - docs/components
        - docs/spring-boot

    - url: https://github.com/apache/camel-karaf.git
      branches:
        - main
        - camel-karaf-3.13.x
        - camel-karaf-3.12.x
        - camel-karaf-3.11.x
        - camel-karaf-3.7.x
      start_path: docs
`

  const config1 = `sourceMap:
  - url: https://github.com/apache/camel-kafka-connector.git
    mappedUrl: ./../camel-kafka-connector
    branches:
      - branch: main
        mapped-branch: main-copy
      - branch: camel-kafka-connector-0.11.x
        mapped-branch: camel-kafka-connector-0.11.x-copy
`

  const config2 = `sourceMap:
  - url: https://github.com/apache/camel-kafka-connector.git
    mappedUrl: ./../camel-kafka-connector
    branches:
      - branch: main
        mapped-branch: main-copy
`

  const config3 = `sourceMap:
  - url: https://github.com/apache/camel.git
    mappedUrl: ./../camel
    branches:
      - branch: main
        mapped-branch: main-copy
`

  it('all branches', () => {
    const playbook = yaml.parse(playbook1)
    const config = camelCaseKeys(yaml.parse(config1), { deep: true })
    sourceMap.register({ config })
    eventEmitter.emit('playbookBuilt', { playbook })
    expect(vars.playbook).to.not.equal(null)
    expect(vars.playbook.content.sources.length).to.equal(playbook.content.sources.length)
    const ckc = vars.playbook.content.sources[8]
    expect(ckc.url).to.equal('./../camel-kafka-connector')
    expect(ckc.branches.length).to.equal(2)
    expect(ckc.branches[0]).to.equal('main-copy')
    expect(ckc.branches[1]).to.equal('camel-kafka-connector-0.11.x-copy')
  })

  it('one branch', () => {
    const playbook = yaml.parse(playbook1)
    const config = camelCaseKeys(yaml.parse(config2), { deep: true })
    sourceMap.register({ config })
    eventEmitter.emit('playbookBuilt', { playbook })
    expect(vars.playbook).to.not.equal(null)
    expect(vars.playbook.content.sources.length).to.equal(playbook.content.sources.length + 1)
    const ckcMapped = vars.playbook.content.sources[8]
    expect(ckcMapped.url).to.equal('./../camel-kafka-connector')
    expect(ckcMapped.branches.length).to.equal(1)
    expect(ckcMapped.branches[0]).to.equal('main-copy')
    const ckc = vars.playbook.content.sources[9]
    expect(ckc.url).to.equal('https://github.com/apache/camel-kafka-connector.git')
    expect(ckc.branches.length).to.equal(1)
    expect(ckc.branches[0]).to.equal('camel-kafka-connector-0.11.x')
  })

  it('multiple source matches', () => {
    const playbook = yaml.parse(playbook1)
    const config = camelCaseKeys(yaml.parse(config3), { deep: true })
    sourceMap.register({ config })
    eventEmitter.emit('playbookBuilt', { playbook })
    expect(vars.playbook).to.not.equal(null)
    expect(vars.playbook.content.sources.length).to.equal(playbook.content.sources.length + 1)
    const ckc0 = vars.playbook.content.sources[0]
    expect(ckc0.url).to.equal('./../camel')
    expect(ckc0.branches.length).to.equal(1)
    expect(ckc0.branches[0]).to.equal('main-copy')
    const ckc1 = vars.playbook.content.sources[1]
    expect(ckc1.url).to.equal('./../camel')
    expect(ckc1.branches.length).to.equal(1)
    expect(ckc1.branches[0]).to.equal('main-copy')
    const ckc2 = vars.playbook.content.sources[2]
    expect(ckc2.url).to.equal('https://github.com/apache/camel.git')
    expect(ckc2.branches.length).to.equal(4)
    expect(ckc2.branches[0]).to.equal('camel-3.13.x')
    const ckc3 = vars.playbook.content.sources[3]
    expect(ckc3.url).to.equal('https://github.com/apache/camel.git')
    expect(ckc3.branches.length).to.equal(1)
    expect(ckc3.branches[0]).to.equal('camel-2.x')
  })
})
